namespace SeoulAir.Device.Domain.Enums
{
    public enum LightColor : byte
    {
        Black,
        Blue,
        Green,
        Yellow,
        Red
    }
}
