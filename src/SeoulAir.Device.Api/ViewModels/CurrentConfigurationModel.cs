﻿namespace SeoulAir.Device.Api.ViewModels
{
    public class CurrentConfigurationModel
    {
        public string Name { get; set; }
        public string ReadingDelayMs { get; set; }
    }
}
